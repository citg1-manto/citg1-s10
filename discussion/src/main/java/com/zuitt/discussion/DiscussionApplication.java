package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {



	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


	// get all users
	@GetMapping("/users")
	public String getAllUser(){
		return "All user retrieved";
	}
    // create all user
	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}
	// get specific user
	@GetMapping("/users/{id}")
	public String getUser(@PathVariable Long id){
		return "Hello "+id;
	}

	// delete specific user
	@DeleteMapping("/users/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Long id, @RequestHeader("Authorization") String user) {
		if (user != null && !user.isEmpty()) {
			// perform delete operation through name
			return ResponseEntity.ok().body("The User " + id + " has been deleted.");
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized access.");
		}
	}
	@PutMapping("/users/{id}")
	@ResponseBody
	public Users updateName(@PathVariable Long id, @RequestBody Users user) {
		// update the name of the user
		return user;
	}
}
